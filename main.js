const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];
const root = document.querySelector("#root");

function booksList(arr) {
    const ulContainer = document.createElement("ul");

    arr.forEach((element) => {
        if (objItem(element) !== undefined) {
        let listItem = document.createElement("li");
        listItem.classList.add("book-item");
        listItem.innerHTML = objItem(element);
        root.appendChild(listItem);
        }
    });

    function objItem(e) {
        try {
            if (e.author === undefined) {
                throw new Error(`Відсутній автор книги - "${e.name}"  `)
            }
            else if (e.name === undefined) {
                throw new Error(`Відсутня назва книги у автора - "${e.author}"`)
            }
            else if (e.price === undefined) {
                throw new Error(`Відсутня ціна книги - "${e.name}"`)
            }
            else {
                return "Автор:" + "&nbsp" + e.author + "&nbsp;&nbsp;&nbsp" + "Назва:"
                    + "&nbsp" + e.name + "&nbsp;&nbsp;&nbsp" + "Ціна" + "&nbsp" + e.price ;
            }
        }

        catch(error) {
             console.log(error.message);
         }
    }
root.appendChild(ulContainer);
}

booksList(books);
